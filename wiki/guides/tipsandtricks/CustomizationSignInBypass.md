## Summary

It's currently possible to access the customization menu in game by editing a memory address on the fly.

*Please note that customization is __not__ possible because all items are still locked.*

## Techincal Breakdown

When viewing FoxGame.upk in UE Explorer Under Class **FoxMenuUI** Line 1805 Function **ui_OpenCustomizationMenu** performs a check on two variables: 

```
if(!Outer.IsPlayerSignedIn() && bForceSignIn)
{
    Properties.DialogType = 1;
    Properties.TitleText = LocalizeMenuString("ArmoryAccessFailTitle");
    Properties.BodyText = LocalizeMenuString("ArmoryAccessFailBody");
    Outer.ShowDialogBox(Properties);
    return;
}
```

If "bForceSignIn" is true the check will fail, bypassing the return and the customization menu will open.

## Implementation

This variable is set at some point after the UDK file is loaded and currently has to be modified at runtime. It can be located by searching for String "ArmoryAccessFailTitle" in memory after the game is loaded.
Search for ```27 07 06 01``` above that string in memory, change the ```27``` to a ```28``` and customization will be accessible.

