# Improving the Wiki

Our wiki is generated using gitlab pages and vuepress.

## Report Issues

If you want to report an issue with the wiki you have to open an issue in
the [BLRevive/Wiki](), chose the template according to your issue from the list below 
and fill out the necessary parts.

- revision
    - false statements
    - broken links/images
    - outdated resources
    - misspells/typing
- improvement
    - readability
    - new informations
- bug
    - website not rendering correctly
    - problems with CI

## Change/Add Content

You have to fork the official repository and pull-request your changes.

All text content is located inside the `/guides` directory.

### Gitlab Web IDE <Badge>Easy</Badge> <Badge type="warning">Limited</Badge>
::: warning limitations
Since Gitlab's markdown parser is not as featured as VuePress one the preview inside the
Gitlab Web IDE is not appropriate. If you want to have access to all features while writing
use the local repository + markdown editor approach!
:::

1. log into gitlab
1. fork https://gitlab.com/blrevive/wiki
1. open web ide in your forked repository
1. after comitting your changes, make a pull request

### Local Repository + Markdown Editor <Badge>Full-Featured</Badge> <Badge type="warning">Advanced</Badge>

You will need the following software installed:

- git
- node.js
- markdown editor of your choice

1. fork https://gitlab.com/blrevive/wiki
1. clone your forked repository
1. install node dependencies (`npm install`)
1. open preview with hot reload (`npm run dev`)
1. make your changes
1. after pushing your changes make pull requesthttps://gitlab.com/blrevive/wiki