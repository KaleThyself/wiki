# Getting Started :video_game:

1. Download [Blacklight: Retribution v3.02](https://steamdb.info/app/209870/)
1. Download the latest [Launcher](https://blrevive.superewald.net/releases/BLRevive-0.4-Preview-5.exe)
1. Setup the Launcher
1. Start play!

::: warning Limitations!
There are currently a few limitations due to the ongoin development.
Most notable the **customization** and **loadouts**  are missing and the user is forced to the default loadout (SMG, AR, SR).
:::

## Launcher Setup

### Register Client

In order to be able to connect to (or host) game servers you need to register your BL:R client
in the launcher.

- go to `Settings`
- either:
    1. drag & drop the `Binaries/Win32/FoxGame-win32-Shipping.exe` into the client list
    1. click on `Add`  beside the client list
        - click on `Browser`  or enter path to `FoxGame-win32-Shipping.exe`
        - click `Save` to register the client

### Connect to Game Servers (Joining a Game)

You can either use our server browser which gives you easy access to running game servers or connect to a specific server by address.

#### Server Browser

- go to `Server Browser`
- right-click on the server list and chose `Update`
- right-click on a server entry in the server list and chose `Connect->3.02`

#### Server Address

- go to `Server Browser`
- at the `Custom Server` section enter the IP and Port of the game server
- click on `Connect`

After connecting to a server the client will start.

### FAQ

#### - **Im getting "Couldn't connect to steam"**
This means that the server you tried connecting to is offline. Try a different server.