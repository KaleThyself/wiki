# User Contribution

If you want to contribute to the repository here's a list of things you can do.

- Spread the word
- Help and Discuss with other users in our [Discord](https://discord.gg/wwj9unRvtN)
- [Improve this Wiki](/guides/wiki/improve-wiki.md)
- [Host game servers](/guides/hosting/game-server)
- [Help with development](/dev/getting-started)
- and **Play**