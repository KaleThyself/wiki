# Contribution Guidelines

All of the BLRevive repositories have some patterns in common to provide a good introduction
and better overview of the project.

## Reporting Issues / Requesting Features

::: tip Search for existing issues first!
Before creating a new issue make sure there is no issue with same content.

You can view open issues at the [BLRevive/Issues](https://gitlab.com/groups/blrevive/-/issues) list (or use the kanban [Board](https://gitlab.com/groups/blrevive/-/boards)).
:::

The repositories have several [issue templates](https://docs.gitlab.com/ee/user/project/description_templates.html#use-the-templates) to suit different scenarios.

| Name | Description | Template |
| --- | --- | --- | --- |
| Bug | An issue which breaks logic or expirience | [.gitlab/issue_templates/Bug.md](https://gitlab.com/blrevive/launcher/-/blob/main/.gitlab/issue_templates/Bug.md) |
| Feature | Request for new features | [.gitlab/issue_templates/Feature.md](https://gitlab.com/blrevive/launcher/-/blob/main/.gitlab/issue_templates/Feature.md) |

Just follow the instructions inside the according template.

## Code Contributions

::: warning  Fork & Merge Request Only!
:hand: All code contributions must be in forked repositories and merged (pull) requested later into the official repository.
:::

### code style

- the repositories will contain isntructions about the code style inside it's `Readme.md#code-style` if necessary

### commits

- try to use small and precise commit messages and reference related issues with `#<id>`.

### merge/pull requests

- add `WIP:` as prefix to the title to indicate that the request isn't finished yet
- similiar to issues, use the according template and follow the instructions
- the pull requests need to be approved by a member of the repository before merging it
- unless explicitly specified in the repositories `Readme.md#request-strategy`, all requests should target the default branch (most likely `main`)

We appreciate your work and will do our best to review and merge them as fast as possible :clap: