---
home: true
heroImage: /blrevive.png
actionText: Play BL:R on PC!
actionLink: /guides/user/getting-started
features:
- title: Getting Started
  details: >
    Start playing BL:R on PC by using our custom launcher
  link:
    text: Start Play
    link: /guides/user/getting-started
- title: Contribute
  details: > 
    Help this project with manpower or resources!
  link:
    text: Contribute
    link: /guides/user/contribution
- title: Diving Deep
  details: >
    Help improving this project as developer!
  link:
    text: Start developing
    link: /guides/dev/getting-started
---

::: slot description
An open source project which enables *Blacklight: Retribution* for PC platform!
:::
